#!/bin/bash
clear
echo Enter a number
read num
n=$num
rev=0
while [ $num -gt 0 ]
        do 
        rem=`expr $num % 10`
        rev=`expr $rev \* 10 + $rem`
        num=`expr $num / 10`
done 
echo Reverse of $n is $rev
if [ $n==$rev ]
        then
        echo it is a Palindrome
else
        echo it is not a Palindrome
fi